﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    public Transform player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.instance.currentPlayer.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < 95)
        Move();
    }

    private void Move()

    {
        if(transform.position.x - player.transform.position.x < 5 && Input.GetButton("HorizontalRight"))
        {
            transform.Translate(Vector2.right * 3 * Time.deltaTime);
        }

        if(transform.position.x <= player.transform.position.x)
        {
            transform.Translate(Vector2.right * 6 * Time.deltaTime);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Goomba")
        {
            collision.gameObject.GetComponent<Goomba>().enabled = true;
        }

        if(collision.gameObject.tag == "Koopa")
            {
            collision.gameObject.GetComponent<Koopa>().enabled = true;
        }
    }

}
