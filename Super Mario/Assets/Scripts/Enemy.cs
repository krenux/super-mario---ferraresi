﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    public bool goLeft;
    public bool canMove;
    protected Vector2 direction;
    protected BoxCollider2D gameObjectCollider;
    public LayerMask terrain;
    public float halfColliderWidth;
    public float halfColliderHeigth;
    public LayerMask player;


    public virtual void Awake()
    {
        gameObjectCollider = gameObject.GetComponent<BoxCollider2D>();
        direction = Vector2.left;
        goLeft = true;
        canMove = true;

        halfColliderWidth = (transform.localScale.x * gameObjectCollider.size.x) / 2 + 0.1f;
        halfColliderHeigth = (transform.localScale.y * gameObjectCollider.size.y) / 2 + 0.1f;
    }

    public virtual void Update()
    {
        if(canMove)
        Move();
    }

    protected void Move()
    {
        if(Physics2D.Raycast(transform.position, Vector2.left, halfColliderWidth + 0.4f, terrain) && goLeft ||
           Physics2D.Raycast(transform.position, Vector2.right, halfColliderWidth + 0.4f, terrain) && !goLeft)
        {
            goLeft = !goLeft;
            direction = -direction;
        }
        transform.Translate(direction * speed * Time.deltaTime); 
    }

}
