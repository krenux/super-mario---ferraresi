﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goomba : Enemy
{

    public void Kill()
    {
        canMove = false;
        Destroy(gameObject, 0.5f);
        //set animation
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.right, halfColliderWidth, player) ||
            Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.left, halfColliderWidth, player) ||
            Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.up, halfColliderHeigth, player))
        {
            if (canMove)
            {
                if (collision.gameObject.tag == "Player")
                {
                    collision.transform.GetComponent<PlayerManager>().TakeDamamge();
                }
            }
        }
    }
}