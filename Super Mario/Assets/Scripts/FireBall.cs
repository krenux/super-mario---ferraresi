﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{

    public float speed = 2;

    public int directionSide = 1;

    private BoxCollider2D fireBallCollider;

    private Vector2 direction;

    private Timer jumpTimer;

    private Rigidbody2D rb;

    public LayerMask terrain = 9;

    public LayerMask enemy = 10;

    private void Awake()
    {
        jumpTimer = new Timer(false, 0.75f, 0.75f);
        fireBallCollider = this.GetComponent<BoxCollider2D>();
        rb = this.gameObject.GetComponent<Rigidbody2D>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        jumpTimer.Update();
    }

    private void Move()
    {
        direction = new Vector2(directionSide, 0);
        transform.Translate(direction * Time.deltaTime * speed);

        Debug.DrawRay(transform.position, new Vector3(fireBallCollider.size.x * -2.5f, 0, 0), Color.red);
    }

    private void Jump()
    {

        if (jumpTimer.Check())
        {

            rb.AddForce(Vector2.up * 150f);
            jumpTimer.Reset();
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 10)
        {
            if (collision.gameObject.tag == "Goomba")
            {
                GameManager.instance.score += 100;
            }
            else if (collision.transform.gameObject.tag == "Koopa")
            {
                GameManager.instance.score += 200;
            }
            Destroy(collision.transform.gameObject);
        }
        else if (collision.gameObject.layer != 10)
        {
            Destroy(gameObject); 
        }

    }
}
