﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PlayerState
{
    miniMario,
    bigMario,
    fireMario,
    invincible
}

public class PlayerManager : MonoBehaviour
{
    public PlayerState state;

    #region boxCollider 2D
    private BoxCollider2D gameObjectCollider;
    #endregion

    private GameObject fireBall;

    private GameObject camera;

    private Rigidbody2D rb;

    private Vector2 direction;

    private Animator marioAnimator;

    #region Timer
    private Timer changeDirection;
    public Timer vulnerable;
    public Timer starStateTimer;
    public Timer shootTimer;
    #endregion

    private RaycastHit2D hit;
    private RaycastHit2D hitBlock;

    public float speed = 3;
    public float jumpForce;
    public float maxHeight;

    public float halfColliderWidth;
    public float halfColliderHeigth;
    private float currentSpeed;
    private float maxHeightValue;

    private int healt = 1;
    [SerializeField]
    private int numberOfEnemyHit;

    #region private Bool Field
    private bool goLeft;
    private bool isJump;
    public bool canJump;
    private bool death;
    private bool canMove;
    #endregion

    #region Layer Mask Field
    public LayerMask player;
    public LayerMask block;
    public LayerMask enemy;
    public LayerMask terrain;
    #endregion

    private void Awake()
    {
        player = 1 << 9;
        player |= 1 << 13;
        player |= 1 << 10;
        player = ~player;

        #region SetReference
        fireBall = GameManager.instance.fireBall;
        gameObjectCollider = gameObject.GetComponent<BoxCollider2D>();
        marioAnimator = gameObject.GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        gameObjectCollider = gameObject.GetComponent<BoxCollider2D>();
        camera = GameManager.instance.camera;
        #endregion

        #region SetTimer
        changeDirection = new Timer(false, 0.3f);
        vulnerable = new Timer(false, 2);
        starStateTimer = new Timer(false, 5, 5);
        shootTimer = new Timer(false, 1, 1);
        #endregion

        canMove = true;

        state = PlayerState.miniMario;


        halfColliderWidth = (transform.localScale.x * gameObjectCollider.size.x) / 2 ;
        halfColliderHeigth = (transform.localScale.y * gameObjectCollider.size.y) / 2 ;

    }
    // Update is called once per frame
    void Update()
    {
        Move();
        Jump();
        Shoot();
        KillEnemy();
        DestroyBlock();
        Run();

        changeDirection.Update();
        vulnerable.Update();
        starStateTimer.Update();
        shootTimer.Update();
    }

    //move player left and right depending last button down
    private void Move()
    {

        if (transform.position.x - camera.transform.position.x <= -13.94f)
            transform.position = new Vector2(camera.transform.position.x - 13.95f, transform.position.y);
        if (Input.GetButtonDown("HorizontalLeft"))
        {
            if(!goLeft)
            marioAnimator.SetBool("ChangeDirection", true);
            goLeft = true;
            transform.localScale = new Vector2(-5, transform.localScale.y);
            //change animation;
        }
        else if (Input.GetButtonDown("HorizontalRight"))
        {
            if(goLeft)
            marioAnimator.SetBool("ChangeDirection", true);
            goLeft = false;
            transform.localScale = new Vector2(5, transform.localScale.y);
            //change animation;
        }

        if (Input.GetButton("HorizontalLeft") && !Physics2D.BoxCast(transform.position, new Vector2(gameObjectCollider.size.x, gameObjectCollider.size.y), 0, Vector2.left, halfColliderWidth +0.1f, terrain) 
            && transform.position.x - camera.transform.position.x > -13.94f )
        {
            marioAnimator.SetBool("ChangeDirection", false);
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            if (currentSpeed < 2)
                currentSpeed += Time.deltaTime;
            marioAnimator.SetFloat("Speed", 1);
        }

        if (Input.GetButton("HorizontalRight") && !Physics2D.BoxCast(transform.position, new Vector2(gameObjectCollider.size.x, gameObjectCollider.size.y), 0, Vector2.right, halfColliderWidth + 0.1f, terrain))
        {
            marioAnimator.SetBool("ChangeDirection", false);
            rb.velocity = new Vector2(speed, rb.velocity.y);
            if (currentSpeed < 2)
                currentSpeed += Time.deltaTime;
            marioAnimator.SetFloat("Speed", 1);

        }

        if (!Input.GetButton("HorizontalLeft") && !Input.GetButton("HorizontalRight") && !isJump)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            marioAnimator.SetFloat("Speed", 0);
            transform.localScale = new Vector2(5, transform.localScale.y);
            marioAnimator.SetBool("ChangeDirection", false);

        }
    }

    //increse and decrease speed if left shift is down or not
    private void Run()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (speed < 6)
                speed += Time.deltaTime;
        }

        else if (!Input.GetKey(KeyCode.LeftShift))
        {
            if (speed > 4)
                speed -= Time.deltaTime;
        }
    }

    private void Jump()
    {

        if (Input.GetKeyDown(KeyCode.Space) && canJump)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            isJump = true;
            marioAnimator.SetBool("Jump", true);
        }

        if (Input.GetKey(KeyCode.Space) && isJump && canJump )
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            marioAnimator.SetBool("Jump", true);
        }


        //if space button Up, he hit block(on his head) or he is at the maximum height go down
        if (Input.GetKeyUp(KeyCode.Space) || transform.position.y >= maxHeightValue || !isJump &&
            Physics2D.BoxCast(transform.position, gameObjectCollider.size, 0, Vector2.up, halfColliderHeigth, player))
        {
            rb.velocity = new Vector2(rb.velocity.x, - jumpForce / 2);
            canJump = false;

        }

        //if touch down he can jump
        if (Physics2D.BoxCast(transform.position, gameObjectCollider.size, 0, Vector2.down, halfColliderHeigth,player ))
        {
            canJump = true;
            maxHeightValue = transform.position.y + maxHeight;
            isJump = false;
            numberOfEnemyHit = 1;
            marioAnimator.SetBool("Jump", false);
        }

        if (!Physics2D.BoxCast(transform.position, gameObjectCollider.size, 0, Vector2.down, halfColliderHeigth, player))
        {
            isJump = true;
            canJump = false;
            marioAnimator.SetBool("Jump", true);
        }
    }

    //method for mario crouch
    private void Crouch()
    {
        if (Input.GetButton("Crouch") && state != PlayerState.miniMario)
        {
            //set animation crouch
        }

        if (Input.GetButtonUp("Crouch") && state != PlayerState.miniMario)
        {
            //set animatio crouch
        }
    }

    private void KillEnemy()
    {
        if (Physics2D.Raycast(new Vector2(transform.position.x - halfColliderWidth + 0.1f, transform.position.y), Vector2.down, halfColliderHeigth + 0.2f, enemy))
        {
            hit = Physics2D.Raycast(new Vector2(transform.position.x - halfColliderWidth + 0.1f, transform.position.y), Vector2.down, halfColliderHeigth + 0.2f, enemy);
        }

        else if (Physics2D.Raycast(new Vector2(transform.position.x + halfColliderWidth -0.1f, transform.position.y), Vector2.down, halfColliderHeigth + 0.2f, enemy))
        {
            hit = Physics2D.Raycast(new Vector2(transform.position.x + halfColliderWidth - 0.1f, transform.position.y), Vector2.down, halfColliderHeigth + 0.2f, enemy);
        }

        if(!Physics2D.Raycast(new Vector2(transform.position.x - halfColliderWidth + 0.1f, transform.position.y), Vector2.down, halfColliderHeigth + 0.2f, enemy) &&
           !Physics2D.Raycast(new Vector2(transform.position.x + halfColliderWidth - 0.1f, transform.position.y), Vector2.down, halfColliderHeigth + 0.2f, enemy))
        {
            hit = Physics2D.Raycast(new Vector2(transform.position.x + halfColliderWidth - 0.3f, transform.position.y), Vector2.down, halfColliderHeigth + 0.2f, enemy);
        }

        if(isJump && hit != false)
        {
            if ( hit.transform.gameObject.tag == "Goomba" && transform.position.y > hit.transform.position.y + hit.transform.gameObject.GetComponent<BoxCollider2D>().size.y * 2.5f)
            {
                rb.velocity = new Vector2(rb.velocity.x, 4);
                if (hit.transform.gameObject.GetComponent<Goomba>().canMove)
                {
                    GameManager.instance.score += 100 * numberOfEnemyHit;
                    numberOfEnemyHit++;
                }
                hit.transform.gameObject.GetComponent<Goomba>().Kill();
            }
        }


    }

    //change state and dimension
    public void ChangeState()
    {
        switch (state)
        {
            case PlayerState.miniMario:
                marioAnimator.SetFloat("StateAnimation", 0);
                marioAnimator.SetBool("ChangeState", true);
                marioAnimator.SetFloat("State", 0);
                gameObjectCollider = gameObject.GetComponent<BoxCollider2D>();
                halfColliderWidth = (transform.localScale.x * gameObjectCollider.size.x) / 2;
                halfColliderHeigth = (transform.localScale.y * gameObjectCollider.size.y) / 2; 
                break;
            case PlayerState.bigMario:
                marioAnimator.SetFloat("StateAnimation", 1);
                marioAnimator.SetBool("ChangeState", true);
                marioAnimator.SetFloat("State", 1);
                gameObjectCollider = gameObject.GetComponent<BoxCollider2D>();
                halfColliderWidth = (transform.localScale.x * gameObjectCollider.size.x) / 2;
                halfColliderHeigth = (transform.localScale.y * gameObjectCollider.size.y);
                break;

            case PlayerState.fireMario:
                marioAnimator.SetFloat("StateAnimation", 2);
                marioAnimator.SetBool("ChangeState", true);
                marioAnimator.SetFloat("State", 2);
                gameObjectCollider = gameObject.GetComponent<BoxCollider2D>();
                halfColliderWidth = (transform.localScale.x * gameObjectCollider.size.x) / 2;
                halfColliderHeigth = (transform.localScale.y * gameObjectCollider.size.y) /2;
                break;
        }
    }

    //collision with item or block
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if it collides with Moshroom change state in BigMario
        if (collision.gameObject.tag == "BigMarioUpgrade")
        {
            if (state != PlayerState.fireMario)
            {
                state = PlayerState.bigMario;
                ChangeState();
                healt++;
            }
            Destroy(collision.gameObject);
            GameManager.instance.score += 1000;
        }

        //if it collides whit flower change state in FireMario
        if (collision.gameObject.tag == "FireMarioUpgrade")
        {
            state = PlayerState.fireMario;
            ChangeState();
            healt++;
            Destroy(collision.gameObject);
            GameManager.instance.score += 1000;
        }

        if(collision.gameObject.tag == "StarUpgrade")
        {
            starStateTimer.Reset();
            Destroy(collision.gameObject);
            GameManager.instance.score += 1000;
        }

        if(collision.gameObject.tag == "1Up")
        {
            GameManager.instance.numberbOfLife++;
        }

        if (collision.gameObject.layer == 10 && !starStateTimer.Check())
        {
            if(collision.gameObject.tag == "Koopa")
            {
                GameManager.instance.score += 200;
            }
            else
            {
                GameManager.instance.score += 100;
            }
            Destroy(collision.gameObject);
        }

        canJump = false;

        if (collision.transform.gameObject.tag == "Koopa")
        {

            if (!collision.transform.gameObject.GetComponent<Koopa>().shield && transform.position.y > collision.transform.position.y + collision.transform.gameObject.GetComponent<BoxCollider2D>().size.y)
            {
                rb.AddForce(Vector3.up * 30f);
                collision.transform.gameObject.GetComponent<Koopa>().ShieldState();
                GameManager.instance.score += 100 * numberOfEnemyHit;
                numberOfEnemyHit++;
            }
            else
            {
                rb.AddForce(Vector3.up * 30f);
                collision.transform.gameObject.GetComponent<Koopa>().ShieldHit(goLeft);
            }
        }
    }


    //if mario is mini death, if mario is invincible !takeDmamge else turn mini
    public void TakeDamamge()
    {
        
        if(vulnerable.Check()&& starStateTimer.Check())
        {
            vulnerable.Reset();
            healt--;
            if (healt > 0 && state != PlayerState.invincible)
            {
                state = PlayerState.miniMario;
                ChangeState();
            }

            else if (healt <= 0)
            {
                canMove = false;
                gameObjectCollider.isTrigger = true;
                rb.AddForce(Vector2.down * 5);
                death = true;
            }
        }
    }

    //restart level when Mario get out of map
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "EndMap")
        {
            SceneManager.LoadScene(0);
        }
    }

    private void DestroyBlock()
    {
        //collision with block
        if (Physics2D.Raycast(transform.position, Vector2.up, gameObjectCollider.size.y * 2.5f + 0.2f, block))
        {
            hitBlock = Physics2D.Raycast(transform.position, Vector2.up, gameObjectCollider.size.y * 2.5f + 0.2f, block);

            //if it collides with a item block active block
            if (hitBlock.transform.gameObject.tag == "CoinBlock" || hitBlock.transform.gameObject.tag == "UpgradeBlock" || hitBlock.transform.tag == "StarBlock")
            {
                hitBlock.transform.gameObject.GetComponent<Animator>().SetBool("IsTouch", true);

                if (hitBlock.transform.gameObject.tag == "CoinBlock" && hitBlock.transform.gameObject.GetComponent<ItemBlock>().canSpawn)
                {
                    GameManager.instance.numberOfCoin++;
                    GameManager.instance.score += 200;
                }
                hitBlock.transform.gameObject.GetComponent<ItemBlock>().SpawnObject();
            }

            //if mario is !minimario, he can destroy all destructible block
            if (hitBlock.transform.gameObject.tag == "DistructibleBlock" && state != PlayerState.miniMario)
            {
                GameManager.instance.score += 50;
                Destroy(hitBlock.transform.gameObject);
            }
        }
    }

    private void Shoot()
    {
        if (state == PlayerState.fireMario && Input.GetKey(KeyCode.LeftControl) && shootTimer.Check())
        {
            if (goLeft)
            {
                GameObject newFireBall = Instantiate(fireBall, new Vector2(transform.position.x - 0.5f, transform.position.y), transform.rotation);
                newFireBall.GetComponent<FireBall>().directionSide = -1;
            }
            else if (!goLeft)
            {
                GameObject newFireBall = Instantiate(fireBall, new Vector2(transform.position.x + 0.5f, transform.position.y), transform.rotation);
                newFireBall.GetComponent<FireBall>().directionSide = 1;
            }
            shootTimer.Reset();
        }
    }
}
