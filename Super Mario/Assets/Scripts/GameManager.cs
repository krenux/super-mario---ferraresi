﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance ;
    public GameObject coins;
    public GameObject fireUpgrade;
    public GameObject bigUpgrade;
    public GameObject invincibleUpgrade;
    public GameObject player;
    public GameObject currentPlayer;
    public GameObject fireBall;
    public GameObject camera;
    public Transform playerSpawnPoint;

    public int numberOfCoin;
    public int score;
    public int numberbOfLife;


    private void Awake()
    {
        instance = this;
        score = 0;
        numberOfCoin = 0;
        numberbOfLife = 3;
        camera = GameObject.Find("MainCamera");
        currentPlayer = GameObject.Instantiate(player, playerSpawnPoint.position, playerSpawnPoint.rotation);
    }

}
