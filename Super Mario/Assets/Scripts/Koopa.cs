﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Koopa : Enemy
{
    private Timer koopaShieldTimer;
    public bool shield;

    new void Awake()
    {
        base.Awake();
        koopaShieldTimer = new Timer(false, 0, 5);
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
        koopaShieldTimer.Update();
    }

    public void ShieldState()
    {
        canMove = false;
        shield = true;
        koopaShieldTimer.Reset();
        //changeAnimation
    }

    public void ShieldHit(bool goLeft)
    {
        if(!canMove)
        {
            if (goLeft)
            {
                direction = Vector2.left;
                this.goLeft = true;
                canMove = true;
                shield = true;
                koopaShieldTimer.Reset();
            }
            else if (!goLeft)
            {
                direction = Vector2.right;
                this.goLeft = false;
                canMove = true;
                shield = true;
                koopaShieldTimer.Reset();
            }
            canMove = true;
            shield = true;
            speed = 6;
        }

        else if(canMove)
        {
            canMove = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (shield)
        {
            if (collision.gameObject.layer == 10)
            {
                Destroy(collision.gameObject);
                GameManager.instance.score += 100;
            }
        }

        if (Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.right, halfColliderWidth, player) ||
            Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.left, halfColliderWidth, player) ||
            Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y), Vector2.up, halfColliderHeigth, player))
            {
            if (canMove)
            {
                if (collision.gameObject.tag == "Player")
                {
                    collision.transform.GetComponent<PlayerManager>().TakeDamamge();
                }
            }
        }
    }
}

