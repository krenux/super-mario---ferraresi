﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class of a timer.

public class Timer
{

    private bool resetOnTargetReach;

    private bool stopped;

    // Current elapsed time.

    private float elapsedTime;

    // Time to reach.

    private float targetTime;

    // Constructor.
    // param startTime    Starting time of the timer
    // param targetTime   Time to reach

    public Timer(bool resetOnTargetReach, float startTime, float targetTime)
    {
        this.resetOnTargetReach = resetOnTargetReach;
        this.elapsedTime = startTime;
        this.targetTime = targetTime;
    }

    // Constructor.
    // param target_t   Time to reach

    public Timer(bool resetOnTargetReach, float targetTime) : this(resetOnTargetReach, 0f, targetTime)
    {
    }

    // Constructor.
    // param target_t   Time to reach

    public Timer(float targetTime) : this(true, 0f, targetTime)
    {
    }

    // Method that returns the elapsed time.

    public float GetElapsedTime()
    {
        return elapsedTime;
    }

    public float GetTargetTime()
    {
        return targetTime;
    }

    // Method that sets the target time.
    // param target_t Time to reach

    public void SetTargetTime(float targetTime)
    {
        this.targetTime = targetTime;
    }

    // Method that resets the timer.

    public void Reset()
    {
        elapsedTime = 0f;
        Resume();
    }

    // Method that updates the timer and checks if the target time is reached.

    public bool UpdateAndCheck()
    {
        Update();
        return Check();
    }

    // Method that when the target time is reached resets the elapsed time and returns true.

    public bool Check()
    {
        if (elapsedTime >= targetTime)
        {
            if (resetOnTargetReach)
                Reset();
            else
                Stop();

            return true;
        }
        else
            return false;
    }

    // Method that increases the elapsed time.

    public void Update()
    {
        if(!stopped)
            elapsedTime += Time.deltaTime;
    }

    public void Stop()
    {
        stopped = true;
    }

    public void Resume()
    {
        stopped = false;
    }

    public float RemainingTime()
    {
        return targetTime - elapsedTime;
    }

}
