﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarUpgrade : MonoBehaviour
{
    public float speed = 2;

    public int directionSide = 1;

    private BoxCollider2D upgradeCollider;

    private Vector2 direction;

    private Timer jumpTimer;

    private Rigidbody2D rb;

    public LayerMask terrain;


    private void Awake()
    {
        jumpTimer = new Timer(false, 1.25f);
        upgradeCollider = this.GetComponent<BoxCollider2D>();
        rb = this.gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        jumpTimer.Update();
        Move();
        Jump();
    }

    private void Move()
    {
        if (Physics2D.Raycast(transform.position, Vector2.right, upgradeCollider.size.x * 2.5f + 0.2f, terrain) && directionSide > 0)
        {
            directionSide = -1;
        }
        if (Physics2D.Raycast(transform.position, Vector2.left, upgradeCollider.size.x * 2.5f + 0.2f, terrain) && directionSide < 0)
        {
            directionSide = 1;
        }

        direction = new Vector2(directionSide, 0);
        transform.Translate(direction * Time.deltaTime * speed);

        Debug.DrawRay(transform.position, new Vector3(upgradeCollider.size.x * -2.5f, 0, 0), Color.red);
    }

    private void Jump()
    {
        
        if(jumpTimer.Check())
        {
            Debug.Log("casa");
            rb.AddForce(Vector2.up * 300f);
            jumpTimer.Reset();
        }
    }
}
