﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveUpgrade : MonoBehaviour
{
    LayerMask item;
    BoxCollider2D itemCollider;
    Vector2 direction;
    int speed = 1;
    int directionSide = 1;

    // Start is called before the first frame update
    void Start()
    {
        itemCollider = this.gameObject.GetComponent<BoxCollider2D>();
        item = 1 << 13;
        item = ~item;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        if (Physics2D.Raycast(transform.position, Vector2.right, itemCollider.size.x / 2 + 0.5f, item) && directionSide > 0)
        {
            directionSide = -1;
        }
        if (Physics2D.Raycast(transform.position, Vector2.left, itemCollider.size.x / 2 + 0.5f, item) && directionSide < 0)
        {
            directionSide = 1;
        }

        direction = new Vector2(directionSide, 0);
        transform.Translate(direction * Time.deltaTime * speed);

        Debug.DrawRay(transform.position, new Vector3(itemCollider.size.x / 2 - 0.2f, 0, 0), Color.red);

    }
}
