﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public Text scoreText;
    public Text lifeText;
    public Text coinText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = GameManager.instance.score.ToString();
        lifeText.text = GameManager.instance.numberbOfLife.ToString();
        coinText.text = GameManager.instance.numberOfCoin.ToString();
    }
}
