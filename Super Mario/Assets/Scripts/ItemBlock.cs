﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBlock : MonoBehaviour
{
    public GameObject fireUpgrade;
    public GameObject bigUpgrade;
    public GameObject invicibleUpgrade;
    public GameObject coins;
    public GameObject player;
    public bool canSpawn = true;

    // Start is called before the first frame update
    void Start()
    {
        fireUpgrade = GameManager.instance.fireUpgrade;
        bigUpgrade = GameManager.instance.bigUpgrade;
        //invicibleUpgrade = GameManager.instance.invincibleUpgrade;
        coins = GameManager.instance.coins;
        player = GameManager.instance.currentPlayer;
        invicibleUpgrade = GameManager.instance.invincibleUpgrade;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SpawnObject()
    { if (canSpawn)
        {
            if (gameObject.tag == "CoinBlock")
            {
                GameObject coin = GameObject.Instantiate(coins, new Vector2(transform.position.x, transform.position.y + 1), transform.rotation);
                Destroy(coin, 0.8f);
            }

            else if (gameObject.tag == "UpgradeBlock")
            {
                if (player.GetComponent<PlayerManager>().state == PlayerState.miniMario)
                {
                    GameObject.Instantiate(bigUpgrade, new Vector2(transform.position.x, transform.position.y + 1f), transform.rotation);
                }
                else
                {
                    GameObject.Instantiate(fireUpgrade, new Vector2(transform.position.x, transform.position.y + 1), transform.rotation);
                }
            }

            else if (gameObject.tag == "StarBlock")
            {
                GameObject.Instantiate(invicibleUpgrade, new Vector2(transform.position.x, transform.position.y + 1f), transform.rotation);
                Destroy(gameObject);
            }
            canSpawn = false;
        }
    }
}
